# Cratic

Easy and flexible backend-as-a-service tool. Allows you:

+ Easy and flexible define your own tables, columns and relations between them as simple as in Google Spreadshits
+ Create forms, which you can share anywhere or embed to any website
+ Create any views for your data. With aggregations, joins, sorting and any constraints
+ Define access rules for your team, restrict access for changing tables, or even specific columns and rows
+ Easy define events handlers using FSM
+ Extend using auto-generated REST API

## Installation

Install using docker:

```
docker run -d cratic/cratic
```

