package cmd

import (
	"github.com/jinzhu/gorm"
	"github.com/spf13/cobra"

	"cratic/cmd/cli/cmd/scheme"
	"cratic/cmd/cli/cmd/user"
)

var rootCmd = &cobra.Command{
	Use:   "cratic",
	Short: "Manage your database",
}

func Execute(db *gorm.DB) error {
	rootCmd.AddCommand(
		scheme.Command(db),
		user.Command(db),
	)

	return rootCmd.Execute()
}
