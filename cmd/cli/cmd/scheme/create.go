package scheme

import (
	"strings"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"

	"cratic/internal/models"
)

type createRepo interface {
	Create(name string) (*models.Scheme, error)
}

func createCommand(r createRepo) *cobra.Command {
	var createCommand = &cobra.Command{
		Use:   "create [scheme name]",
		Short: "create scheme command",
		Long:  `create scheme`,
		Args:  cobra.MinimumNArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			name := strings.Join(args, " ")
			if _, err := r.Create(name); err != nil {
				return err
			}
			log.Info().Msgf("scheme %s created", name)
			return nil
		},
	}

	return createCommand
}
