package scheme

import (
	"github.com/jinzhu/gorm"
	"github.com/spf13/cobra"

	"cratic/internal/repo"
)

func Command(db *gorm.DB) *cobra.Command {
	var schemeCmd = &cobra.Command{
		Use:   "scheme",
		Short: "scheme root command",
		Long:  `You can manage scheme here`,
	}

	schemeRepo := repo.NewSchemeRepository(db)

	schemeCmd.AddCommand(createCommand(schemeRepo))

	return schemeCmd
}
