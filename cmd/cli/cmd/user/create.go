package user

import (
	"errors"

	"github.com/spf13/cobra"
)

func createCommand(r repository) *cobra.Command {
	var password string

	var cmd = &cobra.Command{
		Use:   "create [username]",
		Short: "Create user",
		Long:  `Create user with username`,
		Args:  cobra.MinimumNArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			return errors.New("not implemented")
		},
	}

	cmd.Flags().StringVarP(&password, "password", "p", "", "user's password")

	return cmd
}
