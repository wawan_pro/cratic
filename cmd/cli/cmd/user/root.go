package user

import (
	"github.com/jinzhu/gorm"
	"github.com/spf13/cobra"

	"cratic/internal/repo"
)

type repository interface{}

func Command(db *gorm.DB) *cobra.Command {
	var userCmd = &cobra.Command{
		Use:   "user",
		Short: "Manage users",
		Long:  `You can manage users here`,
	}

	r := repo.NewUserRepository(db)

	userCmd.AddCommand(
		createCommand(r),
	)

	return userCmd
}
