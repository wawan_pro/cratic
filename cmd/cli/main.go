package main

import (
	"cratic/internal/models"
	"log"

	"github.com/caarlos0/env"

	"cratic/cmd/cli/cmd"
	"cratic/internal/db"
)

func main() {
	var dbCfg db.Config
	if err := env.Parse(&dbCfg); err != nil {
		log.Fatalf("can't parse config for db: %v", err)
	}

	log.Printf("config %+v", dbCfg)

	gorm, err := db.New(&dbCfg)
	if err != nil {
		log.Fatalf("can't connect to db: %v", err)
	}
	gorm.AutoMigrate(
		new(models.Scheme),
		new(models.User),
	)

	if err := cmd.Execute(gorm); err != nil {
		log.Fatalf("can't execute command: %v", err)
	}
}
