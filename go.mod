module cratic

go 1.14

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/go-sql-driver/mysql v1.4.1
	github.com/jinzhu/gorm v1.9.12
	github.com/rs/zerolog v1.18.0
	github.com/spf13/cobra v0.0.7
)
