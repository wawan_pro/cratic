package db

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

type Config struct {
	Host     string `env:"DB_HOST"`
	Port     string `env:"DB_PORT"`
	Database string `env:"DB_NAME"`
	Username string `env:"DB_USERNAME"`
	Password string `env:"DB_PASSWORD"`
}

func New(cfg *Config) (*gorm.DB, error) {
	connectUrl := cfg.Username + ":" +
		cfg.Password + "@tcp(" + cfg.Host + ":" + cfg.Port + ")/" +
		cfg.Database + "?charset=utf8mb4&parseTime=True&loc=Local"
	return gorm.Open("mysql", connectUrl)
}
