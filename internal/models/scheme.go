package models

import "github.com/jinzhu/gorm"

type Scheme struct {
	gorm.Model
	Name string
}
