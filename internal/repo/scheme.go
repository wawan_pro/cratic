package repo

import (
	"github.com/jinzhu/gorm"

	"cratic/internal/models"
)

type schemeRepository struct {
	db *gorm.DB
}

func NewSchemeRepository(db *gorm.DB) *schemeRepository {
	return &schemeRepository{
		db: db,
	}
}

func (r *schemeRepository) Create(name string) (*models.Scheme, error) {
	s := &models.Scheme{
		Name: name,
	}
	err := r.db.Create(s).Error
	return s, err
}
